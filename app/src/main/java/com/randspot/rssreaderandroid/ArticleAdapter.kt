package com.randspot.rssreaderandroid

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.prof.rssparser.Article
import com.squareup.picasso.Picasso

class ArticleAdapter(articles: MutableList<Article>, context : Context) : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {
    private val articles = getVisibleArticles(articles, context)
    private var isDesc : Boolean = false

    private fun getVisibleArticles(articles: MutableList<Article>, context : Context) : MutableList<Article> {
        val items = mutableListOf<Article>()

        Storage.init(context)
        val hideViewed = Storage.getHideViewedArticles()

        for (article in articles) {

            // article without guid is invalid
            if (article.guid == null)
                continue

            // add only visible articles
            if (!Storage.hasArticleRemoved(article.guid!!) &&
                !(hideViewed && Storage.hasArticleViewed(article.guid!!))) {
                items.add(article)
            }
        }

        isDesc = Storage.getArticleListSortingDescendant()
        if (isDesc) {
            items.reverse()
        }

        return items
    }

    fun removeAt(pos : Int, context : Context) {
        if (articles.size > pos && pos >= 0) {
            if (articles[pos].guid != null) {
                Storage.init(context)
                Storage.setArticleRemoved(articles[pos].guid!!, true)
            }

            articles.removeAt(pos)
            notifyDataSetChanged()
        }
    }

    fun sortDescendant(desc : Boolean) {
        if (isDesc != desc) {
            articles.reverse()
            isDesc = desc
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rss_row, parent, false))

    override fun getItemCount() = articles.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(articles[position])

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(article: Article) {

            val context = itemView.context

            val imageView = itemView.findViewById<ImageView>(R.id.image_article)
            val titleView = itemView.findViewById<TextView>(R.id.title_article)
            val subtitleView = itemView.findViewById<TextView>(R.id.subtitle_article)
            val viewdFlagImageView = itemView.findViewById<ImageView>(R.id.image_hasViewed)

            Storage.init(context)

            viewdFlagImageView.visibility = if (article.guid != null && Storage.hasArticleViewed(article.guid!!)) View.VISIBLE else View.INVISIBLE
            titleView.text = article.title

            subtitleView.text = String.format("%s - %s", article.author, trimDate(article.pubDate))

            Picasso.get()
                .load(article.image)
                .into(imageView)

            itemView.setOnClickListener {
                // open new activity with WebView
                val intent = Intent(context, WebViewActivity::class.java)
                intent.putExtra("url", article.link)
                intent.putExtra("guid", article.guid)

                context.startActivity(intent)

            }
        }

        private fun trimDate(date : String?) : String {
            if (date == null)
                return ""

            var trimmedDate : String = date
            val index : Int = trimmedDate.lastIndexOf('+')
            if (index >= 0) {
                trimmedDate = trimmedDate.substring(0, index)
            }

            return trimmedDate
        }
    }
}