package com.randspot.rssreaderandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import com.google.android.material.snackbar.Snackbar

class WebViewActivity : AppCompatActivity() {
    private var timer : CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        val webView = findViewById<WebView>(R.id.webview)
        val progress = findViewById<ProgressBar>(R.id.webview_progress)

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progress.visibility = View.GONE
            }
        }

        val url = intent.extras?.getString("url")
        if (url != null) {
            progress.visibility = View.VISIBLE
            webView.loadUrl(url)
        }
        else {
            Snackbar.make(webView, R.string.error_webViewLoad, Snackbar.LENGTH_LONG).show()
        }

        val itemGuid = intent.extras?.getString("guid")
        if (itemGuid != null) {

            Storage.init(this)

            timer = object : CountDownTimer(5000, 5000) {
                override fun onTick(millisUntilFinished: Long) {
                }

                override fun onFinish() {
                    Storage.setArticleViewed(itemGuid, true)
                }
            }
            timer?.start()
        }
    }

    override fun onStop() {
        super.onStop()

        if (timer != null) {
            timer?.cancel()
        }
    }
}
