package com.randspot.rssreaderandroid

import android.content.Context
import android.content.SharedPreferences

object Storage {
    private const val VISIBILITY_PREFIX = "HasViewed_"
    private const val REMOVED_PREFIX = "HasRemoved_"
    private const val LIST_SORTDESC = "SortListDesc"
    private const val LIST_HIDEVIEWED = "HideViewedArticles"

    private var settings: SharedPreferences? = null

    fun init(context : Context) {
        settings = context.getSharedPreferences("com.randspot.rssreaderandroid", Context.MODE_PRIVATE)
    }

    fun clearStorage() {
        settings!!.edit()!!.clear().commit()
    }

    fun hasArticleViewed(guid : String) : Boolean {
        val key = VISIBILITY_PREFIX + guid
        return settings!!.getBoolean(key, false)
    }

    fun setArticleViewed(guid : String, hasViewed : Boolean) {
        val key = VISIBILITY_PREFIX + guid
        settings!!.edit()!!.putBoolean(key, hasViewed).commit()
    }

    fun hasArticleRemoved(guid : String) : Boolean {
        val key = REMOVED_PREFIX + guid
        return settings!!.getBoolean(key, false)
    }

    fun setArticleRemoved(guid : String, hasRemoved : Boolean) {
        val key = REMOVED_PREFIX + guid
        settings!!.edit()!!.putBoolean(key, hasRemoved).commit()
    }

    fun getArticleListSortingDescendant() : Boolean {
        return settings!!.getBoolean(LIST_SORTDESC, false)
    }

    fun setArticleListSortingDescendant(descendant : Boolean) {
        settings!!.edit()!!.putBoolean(LIST_SORTDESC, descendant).commit()
    }

    fun getHideViewedArticles() : Boolean {
        return settings!!.getBoolean(LIST_HIDEVIEWED, false)
    }

    fun setHideViewedArticles(hide : Boolean) {
        settings!!.edit()!!.putBoolean(LIST_HIDEVIEWED, hide).commit()
    }
}