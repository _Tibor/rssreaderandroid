package com.randspot.rssreaderandroid

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.RelativeLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.prof.rssparser.Article

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val rootLayout = findViewById<RelativeLayout>(R.id.root_layout)
        val swipeLayout = findViewById<SwipeRefreshLayout>(R.id.swipe_layout)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)

        // Set ViewModel observers
        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
        viewModel.getArticles().observe(this, Observer<MutableList<Article>>{ articles ->
            val adapter = ArticleAdapter(articles, this)
            recyclerView.adapter = adapter
            adapter.notifyDataSetChanged()

            swipeLayout.isRefreshing = false
            Snackbar.make(rootLayout, R.string.message_rssFeedsUpdated, Snackbar.LENGTH_SHORT).show()
        })

        viewModel.getError().observe( this, Observer<Boolean> { hasError ->
            if (hasError) {
                Snackbar.make(rootLayout, R.string.error_rssFetchFailed, Snackbar.LENGTH_LONG).show()
            }
        })

        // Set Swipe layout
        swipeLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark)
        swipeLayout.canChildScrollUp()
        swipeLayout.setOnRefreshListener {
            swipeLayout.isRefreshing = true
            viewModel.fetchArticles()
        }

        // Recycler View
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.setHasFixedSize(true)

        // Swipe handler - handle delete items to left swipe
        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerView.adapter as ArticleAdapter
                adapter.removeAt(viewHolder.adapterPosition, this.context)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        // fetch RSS feeds
        swipeLayout.isRefreshing = true
        viewModel.fetchArticles()
    }

    override fun onResume() {
        super.onResume()
        redrawList()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        super.onPrepareOptionsMenu(menu)

        if (menu != null) {
            Storage.init(this)
            val sortDesc: Boolean = Storage.getArticleListSortingDescendant()
            val hideViewed: Boolean = Storage.getHideViewedArticles()

            menu.findItem(R.id.action_sortAscendant).isVisible = sortDesc
            menu.findItem(R.id.action_sortDescendant).isVisible = !sortDesc

            menu.findItem(R.id.action_showViewed).isVisible = hideViewed
            menu.findItem(R.id.action_hideViewed).isVisible = !hideViewed
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_sortAscendant -> {
                setSorting(false)
                true
            }
            R.id.action_sortDescendant -> {
                setSorting(true)
                true
            }
            R.id.action_showViewed -> {
                hideViewedArticles(false)
                true
            }
            R.id.action_hideViewed -> {
                hideViewedArticles(true)
                true
            }
            R.id.action_cleanSettings -> {
                Storage.init(this)
                Storage.clearStorage()
                val rootLayout = findViewById<RelativeLayout>(R.id.root_layout)
                Snackbar.make(rootLayout, R.string.message_cleanSettingsSuccess, Snackbar.LENGTH_SHORT).show()

                // fetch RSS feeds
                val swipeLayout = findViewById<SwipeRefreshLayout>(R.id.swipe_layout)
                swipeLayout.isRefreshing = true
                viewModel.fetchArticles()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setSorting(desc : Boolean) {
        Storage.init(this)
        Storage.setArticleListSortingDescendant(desc)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val adapter = recyclerView.adapter as ArticleAdapter

        adapter.sortDescendant(desc)
        adapter.notifyDataSetChanged()
    }

    private fun hideViewedArticles(hide : Boolean) {

        Storage.init(this)
        Storage.setHideViewedArticles(hide)

        redrawList()
    }

    private fun redrawList() {

        if (viewModel.getArticles().value != null) {
            val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
            val adapter = ArticleAdapter(viewModel.getArticles().value!!, this)
            recyclerView.adapter = adapter
            adapter.notifyDataSetChanged()
        }
    }
}
