package com.randspot.rssreaderandroid

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.prof.rssparser.Article
import com.prof.rssparser.Parser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val RSS_URL = "https://www.mojandroid.sk/feed/"

    private lateinit var articles: MutableLiveData<MutableList<Article>>
    private lateinit var error: MutableLiveData<Boolean>

    init {
        if (!::articles.isInitialized) {
            articles = MutableLiveData()
        }

        if (!::error.isInitialized) {
            error = MutableLiveData()
            error.value = false
        }
    }

    fun getArticles(): LiveData<MutableList<Article>> {
        return articles
    }

    fun getError(): LiveData<Boolean> {
        return error
    }

    fun fetchArticles() {
        error.value = false

        viewModelScope.launch(Dispatchers.Main) {
            try {
                val parser = Parser()
                val articleList = parser.getArticles(RSS_URL)
                articles.postValue(articleList)
            } catch (e: Exception) {
                e.printStackTrace()
                articles.postValue(mutableListOf())
                error.postValue(true)
            }
        }
    }
}
